import { ref, onMounted, onBeforeUnmount } from 'vue';
import Swiper from 'swiper';

export function useSwiper() {
  const swiperInstance = ref<Swiper | null>(null);

  onMounted(() => {
    swiperInstance.value = new Swiper('.swiper-container', {
      // Swiper configuration options
      // For example:
      slidesPerView: 3,
      spaceBetween: 30,
      // Add any other Swiper options you need
    });
  });

  onBeforeUnmount(() => {
    if (swiperInstance.value) {
      swiperInstance.value.destroy();
      swiperInstance.value = null;
    }
  });

  return {
    swiperInstance,
  };
}