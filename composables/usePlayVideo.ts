import { onMounted, Ref, ref } from 'vue';

export default function usePlayVideo() {


   const playVideo = (el:Ref<HTMLVideoElement | null>) =>{
        return document.getElementsByTagName(el).play();
   }

    return {
        playVideo
    }
}