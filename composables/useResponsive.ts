import { ref, onMounted, onBeforeUnmount } from 'vue';

export const useResponsive = () => {
    const windowWidth = ref(0);

  function handleResize() {
    windowWidth.value = window.innerWidth;
  }

  // Add window resize event listener on component mount (client-side)
  onMounted(() => {
    if (process.client) {
      windowWidth.value = window.innerWidth;
      window.addEventListener('resize', handleResize);
    }
  });

  // Remove window resize event listener on component unmount (client-side)
  onBeforeUnmount(() => {
    if (process.client) {
      window.removeEventListener('resize', handleResize);
    }
  });

  return {
    windowWidth,
  };
}