import { onMounted, Ref, ref } from 'vue';

export default function useVideoHeight(): {
  videoElement: Ref<HTMLVideoElement | null>;
  videoParent: Ref<HTMLDivElement | null>;
} {
  const videoElement = ref<HTMLVideoElement | null>(null);
  const videoParent = ref<HTMLDivElement | null>(null);

  onMounted(() => {
    if (videoElement.value && videoParent.value) {
      const videoHeight = videoElement.value.clientHeight
      videoParent.value.style.minHeight = `${videoHeight}px`;
    }
  });

  return {
    videoElement,
    videoParent,
  };
}