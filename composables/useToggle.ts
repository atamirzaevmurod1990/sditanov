import { ref } from 'vue';
export const useToggle = () => {
  const isBarOpen = ref(false);

  function toggleBar() {
    isBarOpen.value = !isBarOpen.value;
  }
  
  return {
    isBarOpen,
    toggleBar,
  };
}