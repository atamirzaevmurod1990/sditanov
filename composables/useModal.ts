import { ref } from 'vue';

export default function useModal(){
    const showModal = ref(false);

    function toggleModal(): void {
        showModal.value = !showModal.value;
    }

    return {
        showModal,
        toggleModal
    }

}