// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  
  app: {
    head: {
      htmlAttrs: {
        lang: 'ru-RU',
      },
      title: 'Sditanov Home Page',
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { hid: 'description', name: 'description', content: 'Sditanov Home Program Page' }
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: 'favicon.png' }
      ]
    },
  },
  
  devtools: { enabled: false },

  css: ['~/assets/css/main.css'],

  modules: [
    '@nuxtjs/i18n',
    '@nuxtjs/tailwindcss',
    'nuxt-swiper',
  ],

  i18n:{ 
    lazy: true,
    langDir: 'locales',
    locales:[
      {
        code: 'ru',
        iso: 'ru-RU',
        name: 'Русский',
        file: 'ru-RU.json'
      },
      {
        code: 'en',
        iso: 'en-US',
        name: 'English(US)',
        file: 'en-US.json'
      },
      {
        code: 'uz',
        iso: 'Lt-uz-UZ',
        name: 'Uzbek',
        file: 'uz-UZB.json'
      },
    ],
    strategy: 'prefix',
    defaultLocale: 'ru',
  },

})
